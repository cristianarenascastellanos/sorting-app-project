package com.cristian;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class DefaultArgsTest {
    private String[] args;

    public DefaultArgsTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection params() {
        return Arrays.asList(new Object[][]
                {
                        {new String[]{"1","5","0","7","4"}},
                        {new String[]{"2","1","8","3","1","7"}},
                        {new String[]{"3","8","99","0","4","45","1000"}}
                });
    }

    @Test
    public void testDefaultArgs(){
        String[] originalArgs=args;
        System.out.println("Params: " + Arrays.toString(args));
        App.main(args);
        Arrays.sort(originalArgs);
        Assert.assertArrayEquals(originalArgs,args);
    }
}
