package com.cristian;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class OneArgTest {
    private String[] args;

    public OneArgTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection params() {
        return Arrays.asList(new Object[][]
                {
                        {new String[]{"1"}},
                        {new String[]{"2"}},
                        {new String[]{"3"}}
                });
    }

    @Test
    public void testOneArg(){
        String[] originalArgs=args;
        System.out.println("Params: " + Arrays.toString(args));
        App.main(args);
        Assert.assertArrayEquals(originalArgs,args);
    }
}
