package com.cristian;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class MoreThanTenArgsTest {
    private String[] args;

    public MoreThanTenArgsTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection params() {
        return Arrays.asList(new Object[][]
                {
                        {new String[]{"1","5","0","7","4","8","1","88","0","74","8"}},
                        {new String[]{"2","1","8","3","1","7","3","7","52","1","1","56"}},
                        {new String[]{"3","8","99","0","4","45","1000","8","9","10","78","74","11"}}
                });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgs(){
        App.main(args);
    }
}
