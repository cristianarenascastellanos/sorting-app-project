package com.cristian;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class EmptyArgsTest {
    private String[] args;

    public EmptyArgsTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection params() {
        return Arrays.asList(new Object[][]
                {
                        {new String[0]},
                        {new String[0]},
                        {new String[0]}
                });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyArgs(){
        System.out.println("Params: " + Arrays.toString(args));
        App.main(args);
    }
}
