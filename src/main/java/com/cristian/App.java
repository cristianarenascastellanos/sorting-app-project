package com.cristian;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        if(args.length==0||args.length>10){
            throw new IllegalArgumentException("You must insert more than 0 digits and up to 10 digits");
        }
        int[] arr = new int[args.length];
        try {
            for (int i = 0; i < arr.length; i++) {
               arr[i]=Integer.parseInt(args[i]);
            }
        } catch (Exception e){
            throw new RuntimeException("You can only insert integer values");
        }
        Arrays.sort(arr);
        System.out.println("Sorted digits: " + Arrays.toString(arr));
    }
}
